require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Kurilka
  class Application < Rails::Application
    config.enable_dependency_loading = true
    config.time_zone = 'Moscow'
    config.i18n.default_locale = :ru
    config.i18n.available_locales = :ru

    # TODO: раскомментить, когда настанет Время.
    # Rails.application.credentials.fetch(:secret_key_base) { raise "it seems you didn't configure credentials" }
  end
end
