Rails.application.routes.draw do
  root 'quotes#index'

  resources :quotes, except: [:edit,:update,:destroy] do
    member do
      get :change_rating
    end
  end
end
