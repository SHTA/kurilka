class CreateQuotes < ActiveRecord::Migration[5.2]
  def change
    create_table :quotes do |t|
      t.text :text
      t.integer :rate, default: 0
      t.string :author_nickname, default: 'Anonymous'

      t.timestamps
    end
  end
end
