module ApplicationHelper
  
  # Форматирование даты / времени
  def rus_date date
    date.strftime('%d.%m.%Y') rescue date
  end
  def rus_dtime dtime
    dtime.strftime('%d.%m.%Y %H:%M') rescue dtime
  end
end
