$ ->
  current_scroll_position = $(document).scrollTop()
  elem = $('.js-fixed-menu')
  h_position = 120

  # Фиксирование меню
  fixed_position = ->
    if current_scroll_position >= h_position + elem.height()
      elem.addClass('menu-fixed')

  fixed_position()

  headroom = $('.js-fixed-menu')
  show_menu = ->
    headroom.addClass 'js-fixed-menu--pinned'
    headroom.removeClass 'js-fixed-menu--unpinned'
    $('.js-diamond-fade').fadeIn()

  hide_menu = ->
    headroom.addClass 'js-fixed-menu--unpinned'
    headroom.removeClass 'js-fixed-menu--pinned'

  # Логика по скрытию меню
  $(window).scroll ->
    new_scroll = $(document).scrollTop()
    delta_scroll = new_scroll - current_scroll_position
    delta_scroll_abs = Math.abs(delta_scroll)
    if new_scroll >= h_position
      fixed_position()
      if delta_scroll_abs > 15
        if delta_scroll < 0
          if new_scroll > current_scroll_position
            hide_menu()
          else
            show_menu()
        else
          headroom.removeClass('js-fixed-menu--pinned js-fixed-menu--unpinned')
    else
      headroom.removeClass('js-fixed-menu--pinned js-fixed-menu--unpinned')
      elem.removeClass('menu-fixed')
    current_scroll_position = new_scroll