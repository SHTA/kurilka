$ ->
  window.Scroll ?= {}

  window.Scroll.scrollToTop = (speed = 'fast') ->
    $('html, body').animate({ scrollTop: 0 }, speed)

  $(document).on 'click','#scroll-to-top-button', ->
    window.Scroll.scrollToTop('slow')