$ ->
  window.Alert ?= {}

  window.Alert.alertInit = (type = 'alert',message = 'Соединение с сервером было потеряно. Попробуйте перезагрузить страницу') ->
    $('#flash-alert').remove()
    $div = $('<div />').appendTo('body');
    $div.attr('id', 'flash-alert');
    $('#flash-alert').text message
    $('#flash-alert').attr('class',"flash_color_#{ type }")
    $('#flash-alert').css('opacity',1).animate({opacity: 0}, 10000)
    setTimeout ( ->
      $('#flash-alert').css('display','none')
    ), 10000