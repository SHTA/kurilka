$(document).on 'click','.rate_toggler', ->
  direction = $(@).attr 'data-direction'
  quote__block = $(@).closest('.quote__block')
  quote_id = $(quote__block).attr 'data-quote-id'
  quote__footer_counter = $('.quote__footer_counter',quote__block)
  path = "/quotes/#{ quote_id }/change_rating?direction=#{ direction }"
  $.ajax path,
    method: 'GET'
    success: (json) ->
      if json['type'] == 'error'
        window.Alert.alertInit(type = 'alert', message = json['message'])
      else
        window.Alert.alertInit(type = 'notice', message = json['message'])
        quote__footer_counter.text(json['rate'])
    error: () ->
      window.Alert.alertInit()