class QuotesController < ApplicationController

  def index
    if params[:order] == 'rate'
      @quotes = Quote.order(rate: :desc)
    else
      @quotes = Quote.order(created_at: :desc)
    end
  end

  def new
    @quote = Quote.new
  end

  def create
    @quote = Quote.new(quote_params)
    if @quote.save
      flash.notice = "Цитата добавлена"
      redirect_to root_path
    else
      flash.alert = 'Исправьте ошибки'
      render :new
    end
  end

  def change_rating
    @quote = Quote.find params[:id]
    direction = params[:direction]
    unless ['up','down'].include? direction
      render json: { type: 'error', message: 'Ошибка: не передан параметр direction.' }
    else
      session[:rated_quotes] ||= []
      if session[:rated_quotes].include? @quote.id
        render json: { type: 'error', message: 'Вы уже голосовали за эту цитату.' }
      else
        diff = direction == 'up' ? 1 : -1
        new_rate = @quote.change_rate(diff)
        session[:rated_quotes] << @quote.id
        render json: { type: 'success', message: 'Ваш голос учтен.', rate: new_rate }
      end
    end
  end

  private
  def quote_params
    params.require(:quote).permit(
      :text,
      :rate,
      :author_nickname,
    )
  end

end