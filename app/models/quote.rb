class Quote < ApplicationRecord
  validates_presence_of :text, :author_nickname
  validates_length_of :author_nickname, minimum: 2, maximum: 20, allow_blank: false, message: 'длина никнейма должна быть от 2 до 20 символов'
  validates_length_of :text, minimum: 0, maximum: 500, allow_blank: false, message: 'слишком длинное сообщение'
  
  def change_rate diff
    self.rate += diff
    self.save
    self.rate
  end
end
